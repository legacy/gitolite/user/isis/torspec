<a id="srv-spec.txt-6"></a>

# Discussion

<a id="srv-spec.txt-6.1"></a>

## Why the added complexity from proposal 225? {#why-complexity}

The complexity difference between this proposal and prop225 is in part
because prop225 doesn't specify how the shared random value gets to the
clients. This proposal spends lots of effort specifying how the two shared
random values can always be readily accessible to clients.

<a id="srv-spec.txt-6.2"></a>

## Why do you do a commit-and-reveal protocol in 24 rounds? {#why-rounds}

The reader might be wondering why we span the protocol over the course of a
whole day (24 hours), when only 3 rounds would be sufficient to generate a
shared random value.

We decided to do it this way, because we piggyback on the Tor voting
protocol which also happens every hour.

We could instead only do the shared randomness protocol from 21:00 to 00:00
every day. Or to do it multiple times a day.

However, we decided that since the shared random value needs to be in every
consensus anyway, carrying the commitments/reveals as well will not be a
big problem. Also, this way we give more chances for a failing dirauth to
recover and rejoin the protocol.

<a id="srv-spec.txt-6.3"></a>

## Why can't we recover if the 00:00UTC consensus fails? {#why-no-recovery}

If the 00:00UTC consensus fails, there will be no shared random value for
the whole day. In theory, we could recover by calculating the shared
randomness of the day at 01:00UTC instead. However, the engineering issues
with adding such recovery logic are too great. For example, it's not easy
for an authority who just booted to learn whether a specific consensus
failed to be created.
